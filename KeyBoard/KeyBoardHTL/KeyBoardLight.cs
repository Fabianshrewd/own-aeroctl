﻿using System;
using System.Threading.Tasks;
using System.Threading;
using AeroCtl;

public class KeyBoardLight
{
    public static async Task HTLKeyBoardFunction()
    {
        Aero aero = new Aero();
        IRgbController rgb = aero.Keyboard.Rgb;

        byte r = 0;
        byte g = 255;
        byte b = 0;
        byte r2 = 255;
        byte g2 = 165;
        byte b2 = 0;
        byte r3 = 255;
        byte g3 = 0;
        byte b3 = 0;
        bool showOriginalWhite = false;

        byte[] image = new byte[512];

        for (int i = 0; i < 128; ++i)
        {
            image[4 * i + 0] = (byte)i;
            image[4 * i + 1] = 40;
            image[4 * i + 2] = 255;
            image[4 * i + 3] = 90;
        }

        //for (;;)
        //{
        //H
        image = SetPixel(image, 15, r3, g3, b3);
        image = SetPixel(image, 14, r3, g3, b3);
        image = SetPixel(image, 19, r3, g3, b3);

        image = SetPixel(image, 20, r3, g3, b3);

        image = SetPixel(image, 27, r3, g3, b3);
        image = SetPixel(image, 26, r3, g3, b3);
        image = SetPixel(image, 31, r3, g3, b3);

        //T
        image = SetPixel(image, 39, r, g, b);

        image = SetPixel(image, 45, r, g, b);
        image = SetPixel(image, 51, r, g, b);
        image = SetPixel(image, 44, r, g, b);

        image = SetPixel(image, 49, r, g, b);

        //L
        image = SetPixel(image, 63, r2, g2, b2);
        image = SetPixel(image, 62, r2, g2, b2);
        image = SetPixel(image, 61, r2, g2, b2);

        image = SetPixel(image, 67, r2, g2, b2);
        image = SetPixel(image, 73, r2, g2, b2);


        if (showOriginalWhite)
        {
            Console.WriteLine("white");
            await rgb.SetEffectAsync(new RgbEffect { Type = RgbEffectType.Static, Color = RgbEffectColor.White, Brightness = 51 });
        }
        else
        {
            //Console.WriteLine($"{r}, {g}, {b}");
            await rgb.SetEffectAsync(new RgbEffect { Type = RgbEffectType.Custom0, Brightness = 51 });
            await rgb.SetImageAsync(0, image);
        }
        //}
        Thread.Sleep(315360000);
    }

    static byte[] SetPixel(byte[] image, int position, byte r, byte g, byte b)
    {
        image[4 * position + 0] = (byte)position;
        image[4 * position + 1] = r;
        image[4 * position + 2] = g;
        image[4 * position + 3] = b;
        return image;
    }
}