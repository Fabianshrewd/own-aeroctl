﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using AeroCtl.UI.SoftwareFan;
using Application = System.Windows.Application;

namespace AeroCtl.UI;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	private bool supressShutdownOnClose;

	public AeroController Controller { get; }

	public string GitInfo => $"AeroCtl v{ThisAssembly.Git.BaseTag}";

	public MainWindow(AeroController controller)
	{
		this.Controller = controller;
		this.InitializeComponent();
	}

	protected override void OnStateChanged(EventArgs e)
	{
		base.OnStateChanged(e);

		if (this.WindowState == WindowState.Minimized)
		{
			// Minimizing should close the window, but not exist the app.
			this.supressShutdownOnClose = true;
			this.Close();
		}
	}

	protected override void OnClosing(CancelEventArgs e)
	{
		if (this.supressShutdownOnClose)
			return;

		Application.Current.Shutdown();
	}

	private bool hwEditorOpen;
	private async void onEditHwCurveClicked(object sender, RoutedEventArgs e)
	{
		if (this.hwEditorOpen) return;
		this.hwEditorOpen = true;
		try
		{
			IFanCurve curve;
			FanPoint[] points;

			// Try to read the curve from hardware.
			try
			{
				curve = this.Controller.Aero.Fans.GetFanCurve();
				points = new FanPoint[curve.Count];
				for (int i = 0; i < points.Length; ++i)
					points[i] = await curve.GetPointAsync(i);
			}
			catch (Exception ex)
			{
				this.Controller.FanException = ex;
				return;
			}

			// Open editor.
			FanCurveEditor editor = new FanCurveEditor(points, FanCurveKind.Step);

			editor.CurveApplied += async (_, _) =>
			{
				// Apply curve back to hardware.
				try
				{
					for (int i = 0; i < curve.Count; ++i)
					{
						await curve.SetPointAsync(i, points[i]);
					}
				}
				catch (Exception ex)
				{
					this.Controller.FanException = ex;
				}
			};

			editor.ShowDialog();
		}
		finally
		{
			this.hwEditorOpen = false;
		}
	}

	private void onEditSwCurveClicked(object sender, RoutedEventArgs e)
	{
		FanConfig cfg = new FanConfig(this.Controller.SoftwareFanConfig);
		List<FanPoint> curve = new(cfg.Curve);
		FanCurveEditor editor = new FanCurveEditor(curve, FanCurveKind.Linear);
		editor.CurveApplied += (_, _) =>
		{
			cfg.Curve = curve.ToImmutableArray();
			this.Controller.SoftwareFanConfig = cfg;
		};
		editor.ShowDialog();
	}

	private void onEditSwConfigClicked(object sender, RoutedEventArgs e)
	{
		FanConfig cfg = new FanConfig(this.Controller.SoftwareFanConfig);
		FanConfigEditor editor = new FanConfigEditor(cfg);

		if (editor.ShowDialog() == true)
			this.Controller.SoftwareFanConfig = cfg;
	}

	private async void onResetKeyboardClicked(object sender, RoutedEventArgs e)
	{
		MessageBoxResult messageBoxResult = MessageBox.Show(
			"This will reset all keyboard settings (e.g. RGB LED colors). Are you sure?",
			"Reset keyboard",
			MessageBoxButton.YesNo,
			MessageBoxImage.Question);
		if (messageBoxResult == MessageBoxResult.Yes)
			await this.Controller.ResetKeyboard();
	}

    private void onwhitenKeyBoard(object sender, RoutedEventArgs e)
    {
		Thread t2 = new Thread(() => KeyBoardLight.whiteKeyBoardFunction());
		t2.Start();
    }

	#pragma warning disable CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
    private async void onHTLenKeyBoard(object sender, RoutedEventArgs e)
	#pragma warning restore CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
    {
		Thread t1 = new Thread(()=>KeyBoardLight.HTLKeyBoardFunction());
		t1.Start();
    }

    private void onGitLabLinkClicked(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
	{
		Process.Start(new ProcessStartInfo(e.Uri.ToString())
		{
			UseShellExecute = true
		});
	}

	private void onFanExceptionInfoClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
	{
		Exception ex = this.Controller.FanException;
		if (ex == null)
			return;

		MessageBox.Show(ex.ToString(), "Fan exception");
	}
}