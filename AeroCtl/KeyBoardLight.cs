﻿using System;
using System.Threading.Tasks;
using System.Threading;
using AeroCtl;

public class KeyBoardLight
{
    public static async Task whiteKeyBoardFunction()
    {
        //Controllers
        Aero aero = new Aero();
        IRgbController rgb = aero.Keyboard.Rgb;

        //colors
        easyRGB white = new easyRGB(40, 255, 90);

        //empty image
        byte[] image = new byte[512];

        //Make the image white
        for (int i = 0; i < 128; ++i)
        {
            image[4 * i + 0] = (byte)i;
            image[4 * i + 1] = white.Red;
            image[4 * i + 2] = white.Green;
            image[4 * i + 3] = white.Blue;
        }

        //Set image
        await rgb.SetEffectAsync(new RgbEffect { Type = RgbEffectType.Custom0, Brightness = 51 });
        await rgb.SetImageAsync(0, image);

        //Set function to sleep to make no errors
        Thread.Sleep(315360000);
    }

    public static async Task HTLKeyBoardFunction()
    {
        //Controllers
        Aero aero = new Aero();
        IRgbController rgb = aero.Keyboard.Rgb;

        //colors
        easyRGB white = new easyRGB(40, 255, 90);
        easyRGB rgb1 = new easyRGB(0, 255, 0);
        easyRGB rgb2 = new easyRGB(255, 165, 0);
        easyRGB rgb3 = new easyRGB(255, 0, 0);

        //empty image
        byte[] image = new byte[512];

        //Make the image white
        for (int i = 0; i < 128; ++i)
        {
            image[4 * i + 0] = (byte)i;
            image[4 * i + 1] = white.Red;
            image[4 * i + 2] = white.Green;
            image[4 * i + 3] = white.Blue;
        }

        //H - Cordinates (15, 14, 19, 20, 27, 26, 31)
        //T - Cordinates (39, 45, 51, 44, 49)
        //L - Cordinates (63, 62, 61, 67, 73)
        int[] positions_H = new int[] { 15, 14, 19, 20, 27, 26, 31 };
        int[] positions_T = new int[] { 39, 45, 51, 44, 49 };
        int[] positions_L = new int[] { 63, 62, 61, 67, 73 };

        //Set the positions to the color
        for(int i = 0; i < positions_H.Length; i++) { SetKey(image, positions_H[i], rgb1); }
        for (int i = 0; i < positions_T.Length; i++) { SetKey(image, positions_T[i], rgb2); }
        for (int i = 0; i < positions_L.Length; i++) { SetKey(image, positions_L[i], rgb3); }

        //Set image
        await rgb.SetEffectAsync(new RgbEffect { Type = RgbEffectType.Custom0, Brightness = 51 });
        await rgb.SetImageAsync(0, image);

        //Set function to sleep to make no errors
        Thread.Sleep(315360000);
    }

    static byte[] SetKey(byte[] image, int position, easyRGB rgb)
    {
        image[4 * position + 0] = (byte)position;
        image[4 * position + 1] = rgb.Red;
        image[4 * position + 2] = rgb.Green;
        image[4 * position + 3] = rgb.Blue;
        return image;
    }
}