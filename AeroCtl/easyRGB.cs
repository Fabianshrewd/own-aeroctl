﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace AeroCtl
{
    internal class easyRGB
    {
        //Variables
        private byte _Red;
        private byte _Green;
        private byte _Blue;

        //Konstructor
        public easyRGB(byte red, byte green, byte blue)
        {
            this._Red = red;
            this._Green = green;
            this._Blue = blue;
        }

        //Protected access to variable
        public byte Red
        { 
            get { return _Red; }
        }
        public byte Green
        {
            get { return _Green; }
        }
        public byte Blue
        { 
            get { return _Blue; } 
        }
    }
}
